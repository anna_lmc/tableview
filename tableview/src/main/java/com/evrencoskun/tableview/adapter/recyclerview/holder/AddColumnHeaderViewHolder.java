package com.evrencoskun.tableview.adapter.recyclerview.holder;

import android.support.constraint.ConstraintLayout;
import android.view.View;

import com.evrencoskun.tableview.R;

public class AddColumnHeaderViewHolder extends AbstractViewHolder {

    public final ConstraintLayout constraintLayout;

    public AddColumnHeaderViewHolder(View itemView) {
        super(itemView);
        constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraint_column_add);
    }

    public void bind(int color){
        constraintLayout.setBackgroundColor(color);
    }
}
