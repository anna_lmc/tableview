/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.evrencoskun.tableview.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.evrencoskun.tableview.ITableView;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.adapter.recyclerview.CellRecyclerViewAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.ColumnHeaderRecyclerViewAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.RowHeaderRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evrencoskun on 10/06/2017.
 */

public abstract class AbstractTableAdapter<CH, RH, C> implements ITableAdapter {

    private int mRowHeaderWidth;
    private int mColumnHeaderHeight;

    protected Context mContext;
    private ColumnHeaderRecyclerViewAdapter mColumnHeaderRecyclerViewAdapter;
    private RowHeaderRecyclerViewAdapter mRowHeaderRecyclerViewAdapter;
    private CellRecyclerViewAdapter mCellRecyclerViewAdapter;
    private View mCornerView;

    protected List<CH> mColumnHeaderItems;
    protected List<RH> mRowHeaderItems;
    protected List<List<C>> mCellItems;


    private ITableView mTableView;
    private List<AdapterDataSetChangedListener> dataSetChangedListeners;

    public AbstractTableAdapter(Context context) {
        mContext = context;
    }

//    public AbstractTableAdapter(Context context, int rowHeaderWidth, int columnHeaderHeight) {
//        mContext = context;
//        this.mRowHeaderWidth = rowHeaderWidth;
//        this.mColumnHeaderHeight = columnHeaderHeight;
//    }

    public void setTableView(TableView tableView) {
        mTableView = tableView;
        initialize();
    }

    private void initialize() {

        // Create Column header RecyclerView Adapter
        mColumnHeaderRecyclerViewAdapter = new ColumnHeaderRecyclerViewAdapter(mContext,
                mColumnHeaderItems, this);

        // Create Row Header RecyclerView Adapter
        mRowHeaderRecyclerViewAdapter = new RowHeaderRecyclerViewAdapter(mContext,
                mRowHeaderItems, this);
        // Create Cell RecyclerView Adapter
        mCellRecyclerViewAdapter = new CellRecyclerViewAdapter(mContext, mCellItems, mTableView);

    }

    public void setColumnHeaderItems(List<CH> columnHeaderItems) {
        if (columnHeaderItems == null) {
            return;
        }

        mColumnHeaderItems = columnHeaderItems;
        // Invalidate the cached widths for letting the view measure the cells width
        // from scratch.

        // mColumnHeaderRecyclerViewAdapter.setWidth(calculateColumnWidth(mColumnHeaderItems.size()));
        mTableView.getColumnHeaderLayoutManager().clearCachedWidths();
        // calculate new width

        // Set the items to the adapter
        mColumnHeaderRecyclerViewAdapter.setItems(mColumnHeaderItems);
        dispatchColumnHeaderDataSetChangesToListeners(columnHeaderItems);

    }

    public void setRowHeaderItems(List<RH> rowHeaderItems) {
        if (rowHeaderItems == null) {
            return;
        }

        mRowHeaderItems = rowHeaderItems;

        // Set the items to the adapter
        mRowHeaderRecyclerViewAdapter.setItems(mRowHeaderItems);
        dispatchRowHeaderDataSetChangesToListeners(mRowHeaderItems);
    }

    public void addMoreRowHeaderItems(List<RH> rowHeaderItems) {
        if (rowHeaderItems == null) {
            return;
        }
        if (mRowHeaderItems == null)
            mRowHeaderItems = new ArrayList<>();
        mRowHeaderItems.addAll(rowHeaderItems);

        // Set the items to the adapter
        mRowHeaderRecyclerViewAdapter.setItems(mRowHeaderItems);
        dispatchRowHeaderDataSetChangesToListeners(mRowHeaderItems);
    }

    public void setCellItems(List<List<C>> cellItems) {
        if (cellItems == null) {
            return;
        }

        mCellItems = cellItems;
        // Invalidate the cached widths for letting the view measure the cells width
        // from scratch.
        mTableView.getCellLayoutManager().clearCachedWidths();
        // Set the items to the adapter
        mCellRecyclerViewAdapter.setItems(mCellItems);
        dispatchCellDataSetChangesToListeners(mCellItems);
    }

    public void updateCellItems() {
        if (mCellItems != null)
            dispatchCellDataSetChangesToListeners(mCellItems);
    }

    public void addMoreCellItems(List<List<C>> cellItems, List<RH> rowHeaders) {
        if (cellItems == null) {
            return;
        }

        if (mCellItems == null)
            mCellItems = new ArrayList<>();

        mCellItems.addAll(cellItems);
        // Invalidate the cached widths for letting the view measure the cells width
        // from scratch.
        mTableView.getCellLayoutManager().clearCachedWidths();
        // Set the items to the adapter
        mCellRecyclerViewAdapter.setItems(mCellItems);
        addMoreRowHeaderItems(rowHeaders);
        dispatchCellDataSetChangesToListeners(mCellItems);
    }

    public void addHistoryCellItems(List<List<C>> cellItems, List<RH> rowHeaders) {
        if (cellItems == null) {
            return;
        }
        if (mCellItems == null)
            mCellItems = new ArrayList<>();

        mCellItems.addAll(0, cellItems);

        // Invalidate the cached widths for letting the view measure the cells width
        // from scratch.

        // Set the items to the adapter
        mCellRecyclerViewAdapter.setItems(mCellItems);
        addHistoryRowHeaderItems(rowHeaders);
        dispatchCellDataSetChangesToListeners(mCellItems);
    }

    public void addHistoryRowHeaderItems(List<RH> rowHeaderItems) {
        if (rowHeaderItems == null) {
            return;
        }
        if (mRowHeaderItems == null)
            mRowHeaderItems = new ArrayList<>();

        mRowHeaderItems.addAll(0, rowHeaderItems);
        // Set the items to the adapter
        mRowHeaderRecyclerViewAdapter.setItems(mRowHeaderItems);
        dispatchRowHeaderDataSetChangesToListeners(mRowHeaderItems);
    }

//    public void addHistoryPage(List<Event> list) {
//        List<Event> newList;
//        if (this.items != null) {
//            newList = new ArrayList<>();
//            newList.addAll(list);
//            newList.addAll(items);
//            items = newList;
//        } else {
//            this.items = list;
//        }
//        submit(items);
//    }

    public void addMoreInitCellItems(List<List<C>> cellItems, List<RH> rowHeaders) {
        if (cellItems == null) {
            return;
        }

        if (mCellItems == null)
            mCellItems = new ArrayList<>();
        else
            mCellItems.clear();

        mCellItems.addAll(cellItems);
        // Set the items to the adapter
        mCellRecyclerViewAdapter.setItems(mCellItems);

        dispatchCellDataSetChangesToListeners(mCellItems);

        addMoreInitRowHeaderItems(rowHeaders);
    }

    public void addMoreInitRowHeaderItems(List<RH> rowHeaderItems) {
        if (rowHeaderItems == null) {
            return;
        }
        if (mRowHeaderItems == null)
            mRowHeaderItems = new ArrayList<>();
        else
            mRowHeaderItems.clear();
        mRowHeaderItems.addAll(rowHeaderItems);

        // Set the items to the adapter
        mRowHeaderRecyclerViewAdapter.setItems(mRowHeaderItems);
        dispatchRowHeaderDataSetChangesToListeners(mRowHeaderItems);
    }


    public void setAllItems(List<CH> columnHeaderItems, List<RH> rowHeaderItems, List<List<C>>
            cellItems) {

        // Set all items
        setColumnHeaderItems(columnHeaderItems);
        setRowHeaderItems(rowHeaderItems);
        setCellItems(cellItems);

        // Control corner view
        if ((columnHeaderItems != null && !columnHeaderItems.isEmpty()) && (rowHeaderItems !=
                null && !rowHeaderItems.isEmpty()) && (cellItems != null && !cellItems.isEmpty())
                && mTableView != null && mCornerView == null) {

            // Create corner view
            mCornerView = onCreateCornerView();
            mTableView.addView(mCornerView, new FrameLayout.LayoutParams(mRowHeaderWidth, mColumnHeaderHeight));
        } else if (mCornerView != null) {

            // Change corner view visibility
            if (rowHeaderItems != null && !rowHeaderItems.isEmpty()) {
                mCornerView.setVisibility(View.VISIBLE);
            } else {
                mCornerView.setVisibility(View.GONE);
            }
        }
    }

    public View getCornerView() {
        return mCornerView;
    }

    public ColumnHeaderRecyclerViewAdapter getColumnHeaderRecyclerViewAdapter() {

        return mColumnHeaderRecyclerViewAdapter;
    }

    public RowHeaderRecyclerViewAdapter getRowHeaderRecyclerViewAdapter() {
        return mRowHeaderRecyclerViewAdapter;
    }

    public CellRecyclerViewAdapter getCellRecyclerViewAdapter() {
        return mCellRecyclerViewAdapter;
    }

    public void setRowHeaderWidth(int rowHeaderWidth) {
        this.mRowHeaderWidth = rowHeaderWidth;

        if (mCornerView != null) {
            ViewGroup.LayoutParams layoutParams = mCornerView.getLayoutParams();
            layoutParams.width = rowHeaderWidth;
        }
    }

    public int getmRowHeaderWidth() {
        return mRowHeaderWidth;
    }

    public void setColumnHeaderHeight(int columnHeaderHeight) {
        this.mColumnHeaderHeight = columnHeaderHeight;

        if (mCornerView != null) {
            ViewGroup.LayoutParams layoutParams = mCornerView.getLayoutParams();
            layoutParams.height = columnHeaderHeight;
        }
    }

    public int getmColumnHeaderHeight() {
        return mColumnHeaderHeight;
    }


    public CH getColumnHeaderItem(int position) {
        if ((mColumnHeaderItems == null || mColumnHeaderItems.isEmpty()) || position < 0 ||
                position >= mColumnHeaderItems.size()) {
            return null;
        }
        return mColumnHeaderItems.get(position);
    }

    public RH getRowHeaderItem(int position) {
        if ((mRowHeaderItems == null || mRowHeaderItems.isEmpty()) || position < 0 || position >=
                mRowHeaderItems.size()) {
            return null;
        }
        return mRowHeaderItems.get(position);
    }

    public C getCellItem(int columnPosition, int rowPosition) {
        if ((mCellItems == null || mCellItems.isEmpty()) || columnPosition < 0 || rowPosition >=
                mCellItems.size() || mCellItems.get(rowPosition) == null || rowPosition < 0 ||
                columnPosition >= mCellItems.get(rowPosition).size()) {
            return null;
        }

        return mCellItems.get(rowPosition).get(columnPosition);
    }

    public List<C> getCellRowItems(int rowPosition) {
        return (List<C>) mCellRecyclerViewAdapter.getItem(rowPosition);
    }

    public void removeRow(int rowPosition) {
        mCellRecyclerViewAdapter.deleteItem(rowPosition);
        mRowHeaderRecyclerViewAdapter.deleteItem(rowPosition);
    }

    public void removeRow(int rowPosition, boolean updateRowHeader) {
        mCellRecyclerViewAdapter.deleteItem(rowPosition);

        // To be able update the row header data
        if (updateRowHeader) {
            rowPosition = mRowHeaderRecyclerViewAdapter.getItemCount() - 1;

            // Cell RecyclerView items should be notified.
            // Because, other items stores the old row position.
            mCellRecyclerViewAdapter.notifyDataSetChanged();
        }

        mRowHeaderRecyclerViewAdapter.deleteItem(rowPosition);

    }

    public void removeRowRange(int rowPositionStart, int itemCount) {
        mCellRecyclerViewAdapter.deleteItemRange(rowPositionStart, itemCount);
        mRowHeaderRecyclerViewAdapter.deleteItemRange(rowPositionStart, itemCount);
    }

    public void removeRowRange(int rowPositionStart, int itemCount, boolean updateRowHeader) {
        mCellRecyclerViewAdapter.deleteItemRange(rowPositionStart, itemCount);

        // To be able update the row header data sets
        if (updateRowHeader) {
            rowPositionStart = mRowHeaderRecyclerViewAdapter.getItemCount() - 1 - itemCount;

            // Cell RecyclerView items should be notified.
            // Because, other items stores the old row position.
            mCellRecyclerViewAdapter.notifyDataSetChanged();
        }

        mRowHeaderRecyclerViewAdapter.deleteItemRange(rowPositionStart, itemCount);
    }

    public void addRow(int rowPosition, RH rowHeaderItem, List<C> cellItems) {
        mCellRecyclerViewAdapter.addItem(rowPosition, cellItems);
        mRowHeaderRecyclerViewAdapter.addItem(rowPosition, rowHeaderItem);
    }

    public void addRowRange(int rowPositionStart, List<RH> rowHeaderItem, List<List<C>> cellItems) {
        mRowHeaderRecyclerViewAdapter.addItemRange(rowPositionStart, rowHeaderItem);
        mCellRecyclerViewAdapter.addItemRange(rowPositionStart, cellItems);
    }

    public void changeRowHeaderItem(int rowPosition, RH rowHeaderModel) {
        mRowHeaderRecyclerViewAdapter.changeItem(rowPosition, rowHeaderModel);
    }

    public void changeRowHeaderItemRange(int rowPositionStart, List<RH> rowHeaderModelList) {
        mRowHeaderRecyclerViewAdapter.changeItemRange(rowPositionStart, rowHeaderModelList);
    }

    public void changeCellItem(int columnPosition, int rowPosition, C cellModel) {
        List<C> cellItems = (List<C>) mCellRecyclerViewAdapter.getItem(rowPosition);
        if (cellItems != null && cellItems.size() > columnPosition) {
            // Update cell row items.
            cellItems.set(columnPosition, cellModel);

            mCellRecyclerViewAdapter.changeItem(rowPosition, cellItems);
        }
    }

    public void changeColumnHeader(int columnPosition, CH columnHeaderModel) {
        mColumnHeaderRecyclerViewAdapter.changeItem(columnPosition, columnHeaderModel);
    }

    public void changeColumnHeaderRange(int columnPositionStart, List<CH> columnHeaderModelList) {
        mColumnHeaderRecyclerViewAdapter.changeItemRange(columnPositionStart,
                columnHeaderModelList);
    }


    public List<C> getCellColumnItems(int columnPosition) {
        return mCellRecyclerViewAdapter.getColumnItems(columnPosition);
    }

    public void removeColumn(int columnPosition) {
        mColumnHeaderRecyclerViewAdapter.deleteItem(columnPosition);
        mCellRecyclerViewAdapter.removeColumnItems(columnPosition);
    }


    public void addColumn(int columnPosition, CH columnHeaderItem, List<C> cellItems) {
        mColumnHeaderRecyclerViewAdapter.addItem(columnPosition, columnHeaderItem);
        mCellRecyclerViewAdapter.addColumnItems(columnPosition, cellItems);
    }


    public void addNewColumn(CH columnHeaderItem, List<C> cellItems) {
        mColumnHeaderItems.add(columnHeaderItem);

        List<List<C>> items = new ArrayList<>();
        for (int i = 0; i < mCellItems.size(); i++) {
            List<C> rowList = new ArrayList<>((List<C>) mCellItems.get(i));

            rowList.add(cellItems.get(i));
            items.add(rowList);
        }
        mCellItems = items;

        setAllItems(mColumnHeaderItems, mRowHeaderItems, mCellItems);
    }


    public final void notifyDataSetChanged() {
        mColumnHeaderRecyclerViewAdapter.notifyDataSetChanged();
        mRowHeaderRecyclerViewAdapter.notifyDataSetChanged();
        mCellRecyclerViewAdapter.notifyCellDataSetChanged();
    }


    @Override
    public ITableView getTableView() {
        return mTableView;
    }

    @SuppressWarnings("unchecked")
    private void dispatchColumnHeaderDataSetChangesToListeners(List<CH> newColumnHeaderItems) {
        if (dataSetChangedListeners != null) {
            for (AdapterDataSetChangedListener listener : dataSetChangedListeners) {
                listener.onColumnHeaderItemsChanged(newColumnHeaderItems);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void dispatchRowHeaderDataSetChangesToListeners(final List<RH> newRowHeaderItems) {
        if (dataSetChangedListeners != null) {
            for (AdapterDataSetChangedListener listener : dataSetChangedListeners) {
                listener.onRowHeaderItemsChanged(newRowHeaderItems);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void dispatchCellDataSetChangesToListeners(List<List<C>> newCellItems) {
        if (dataSetChangedListeners != null) {
            for (AdapterDataSetChangedListener listener : dataSetChangedListeners) {
                listener.onCellItemsChanged(newCellItems);
            }
        }
    }

    /**
     * Sets the listener for changes of data set on the TableView.
     *
     * @param listener The AdapterDataSetChangedListener listener.
     */
    public void addAdapterDataSetChangedListener(AdapterDataSetChangedListener listener) {
        if (dataSetChangedListeners == null) {
            dataSetChangedListeners = new ArrayList<>();
        }
        dataSetChangedListeners.add(listener);
    }

    public RH getLastRowHeaderItem() {
        if ((mRowHeaderItems == null || mRowHeaderItems.isEmpty())) {
            return null;
        }
        int position = mRowHeaderItems.size() - 1;
        return mRowHeaderItems.get(position);
    }

    public RH getFirtsRowHeaderItem() {
        if ((mRowHeaderItems == null || mRowHeaderItems.isEmpty())) {
            return null;
        }
        int position = 0;
        return mRowHeaderItems.get(position);
    }

    public int getRowsCount() {
        if ((mRowHeaderItems == null || mRowHeaderItems.isEmpty())) {
            return 0;
        }
        return mRowHeaderItems.size();
    }

    public int getCellCount() {
        if ((mCellItems == null || mCellItems.isEmpty())) {
            return 0;
        }
        return mCellItems.size();
    }

    public int getColumnCount() {
        if ((mColumnHeaderItems == null || mColumnHeaderItems.isEmpty())) {
            return 0;
        }
        return mColumnHeaderItems.size();
    }

    public List<CH> getColumns() {
        return mColumnHeaderItems;
    }


    public void removeCell(String id, String date) {

    }

//     if (cellItems == null) {
//        return;
//    }
//
//        if (mCellItems == null)
//    mCellItems = new ArrayList<>();
//
//        mCellItems.addAll(cellItems);
//    // Invalidate the cached widths for letting the view measure the cells width
//    // from scratch.
//        mTableView.getCellLayoutManager().clearCachedWidths();
//    // Set the items to the adapter
//        mCellRecyclerViewAdapter.setItems(mCellItems);
//    addMoreRowHeaderItems(rowHeaders);
//    dispatchCellDataSetChangesToListeners(mCellItems);

    public void insertMoreCellItems(int start, int end, List<List<C>> cellItems) {
        if (cellItems == null) {
            return;
        }
        if (mCellItems == null)
            mCellItems = new ArrayList<>();

        List<List<C>> interval1 = mCellItems.subList(0, start);
        List<List<C>> interval2 = null;
        if (mCellItems.size() > end)
            interval2 = mCellItems.subList(end, mCellItems.size());
        mCellItems.clear();
        mCellItems.addAll(interval1);
        mCellItems.addAll(cellItems);
        if (interval2 != null)
            mCellItems.addAll(interval2);
        mCellRecyclerViewAdapter.setItems(mCellItems);
        dispatchCellDataSetChangesToListeners(mCellItems);
    }

    public void insertMoreRowItems(int start, int end, List<RH> rowItems) {
        if (rowItems == null) {
            return;
        }
        if (mRowHeaderItems == null)
            mRowHeaderItems = new ArrayList<>();

        List<RH> interval1 = mRowHeaderItems.subList(0, start);
        List<RH> interval2 = null;
        if (mRowHeaderItems.size() > end)
            interval2 = mRowHeaderItems.subList(end, mRowHeaderItems.size());
        mRowHeaderItems.clear();
        mRowHeaderItems.addAll(interval1);
        mRowHeaderItems.addAll(rowItems);
        if (interval2 != null)
            mRowHeaderItems.addAll(interval2);
        mRowHeaderRecyclerViewAdapter.setItems(mRowHeaderItems);
        dispatchRowHeaderDataSetChangesToListeners(mRowHeaderItems);
    }

    public void insertMoreItems(int start, int end, List<List<C>> cellItems, List<RH> rowItems) {
        insertMoreCellItems(start, end, cellItems);
        insertMoreRowItems(start, end, rowItems);
    }


    public int getCellEmptyCount() {
        return mCellRecyclerViewAdapter.getRowsEmptyCount();
    }

    public void setCellEmptyCount(int cellEmptyCount) {
        mCellRecyclerViewAdapter.setRowsEmptyCount(cellEmptyCount);

    }
}
