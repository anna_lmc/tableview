/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.evrencoskun.tableview.adapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.ITableView;
import com.evrencoskun.tableview.adapter.ITableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder.SelectionState;
import com.evrencoskun.tableview.helpers.CalculationCellSize;
import com.evrencoskun.tableview.helpers.Size;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

/**
 * Created by evrencoskun on 10/06/2017.
 */

public class CellRowRecyclerViewAdapter<C> extends AbstractRecyclerViewAdapter<C> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

    private static final String LOG_TAG = CellRowRecyclerViewAdapter.class.getSimpleName();

    private int mYPosition;
    private ITableAdapter mTableAdapter;
    private ITableView mTableView;

    public CellRowRecyclerViewAdapter(Context context, ITableView tableView) {
        super(context, null);
        this.mTableAdapter = tableView.getAdapter();
        this.mTableView = tableView;
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AbstractViewHolder viewHolder;
        viewHolder = mTableAdapter.onCreateCellViewHolder(parent, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AbstractViewHolder holder, final int xPosition) {
        if (holder.getItemViewType() != -1) {
            mTableAdapter.onBindCellViewHolder(holder, getItem(xPosition), xPosition, mYPosition);
        } else {
            mTableAdapter.onBindCellViewHolder(holder, xPosition, mYPosition);
        }
    }

    public int getYPosition() {
        return mYPosition;
    }

    public void setYPosition(int rowPosition) {
        mYPosition = rowPosition;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return -1;
        }
        return mTableAdapter.getCellItemViewType(position);
    }

    // STICKY HEADER

    @Override
    public long getHeaderId(int position) {
        return mYPosition;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return mTableAdapter.onCreateColumnStickyViewHolder(parent);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

        mTableAdapter.onBindColumnStickyViewHolder(holder, position, mYPosition);
    }


    public void onForceBindHeader(RecyclerView.ViewHolder holder, int position) {
        mTableAdapter.onBindColumnStickyViewHolder(holder, position, mYPosition);
    }

    @Override
    public void onTranslateHeaderViewHolder(View headerCurrent, View headerNew) {

    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public void onViewAttachedToWindow(AbstractViewHolder viewHolder) {
        super.onViewAttachedToWindow(viewHolder);

        SelectionState selectionState = mTableView.getSelectionHandler().getCellSelectionState
                (viewHolder.getAdapterPosition(), mYPosition);

        // Control to ignore selection color
        if (!mTableView.isIgnoreSelectionColors()) {

            // Change the background color of the view considering selected row/cell position.
            if (selectionState == SelectionState.SELECTED) {
                viewHolder.setBackgroundColor(mTableView.getSelectedColor());
            } else {
                viewHolder.setBackgroundColor(mTableView.getUnSelectedColor());
            }
        }

        // Change selection status
        viewHolder.setSelected(selectionState);
        setHolderLayoutParams(viewHolder);
    }

    @Override
    public boolean onFailedToRecycleView(AbstractViewHolder holder) {
        return holder.onFailedToRecycleView();
    }

    @Override
    public void onViewRecycled(AbstractViewHolder holder) {
        super.onViewRecycled(holder);
        holder.onViewRecycled();
    }

    private void setHolderLayoutParams(AbstractViewHolder holder) {
        Size size = CalculationCellSize.getCellSize(mContext, getItems().size());

        holder.itemView.getLayoutParams().width = size.getWidth();
        holder.itemView.getLayoutParams().height = size.getHeight();
    }
}
