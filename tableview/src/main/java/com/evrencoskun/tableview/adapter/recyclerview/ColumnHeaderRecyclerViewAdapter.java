/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.evrencoskun.tableview.adapter.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.ITableView;
import com.evrencoskun.tableview.R;
import com.evrencoskun.tableview.adapter.ITableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder.SelectionState;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AddColumnHeaderViewHolder;
import com.evrencoskun.tableview.helpers.CalculationCellSize;
import com.evrencoskun.tableview.helpers.Size;

import java.util.List;

/**
 * Created by evrencoskun on 10/06/2017.
 */

public class ColumnHeaderRecyclerViewAdapter<CH> extends AbstractRecyclerViewAdapter<CH>  {
    private static final String TAG = ColumnHeaderRecyclerViewAdapter.class.getSimpleName();

    private ITableAdapter mTableAdapter;
    private ITableView mTableView;

    public ColumnHeaderRecyclerViewAdapter(Context context, List<CH> itemList,
                                           ITableAdapter tableAdapter) {
        super(context, itemList);
        this.mTableAdapter = tableAdapter;
        this.mTableView = tableAdapter.getTableView();
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AbstractViewHolder viewHolder;
        if (viewType == -1) {
            View layout = LayoutInflater.from(mContext).inflate(R.layout.item_column_header_add, parent, false);
            viewHolder = new AddColumnHeaderViewHolder(layout);
        } else {
            viewHolder = mTableAdapter.onCreateColumnHeaderViewHolder(parent, viewType);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AbstractViewHolder holder, int position) {
        if (holder.getItemViewType() != -1) {
            mTableAdapter.onBindColumnHeaderViewHolder(holder, getItem(position), position);
        } else {
            mTableAdapter.onBindColumnHeaderViewHolder(holder, position);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return -1;
        }
        return mTableAdapter.getColumnHeaderItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull AbstractViewHolder viewHolder) {
        super.onViewAttachedToWindow(viewHolder);

        SelectionState selectionState = mTableView.getSelectionHandler().getColumnSelectionState(viewHolder.getAdapterPosition());

        // Control to ignore selection color
        if (!mTableView.isIgnoreSelectionColors()) {

            // Change background color of the view considering it's selected state
            mTableView.getSelectionHandler().changeColumnBackgroundColorBySelectionStatus
                    (viewHolder, selectionState);
        }

        // Change selection status
        viewHolder.setSelected(selectionState);

        // Control whether the TableView is sortable or not.
        setHolderLayoutParams(viewHolder);
    }

    private void setHolderLayoutParams(AbstractViewHolder holder) {

        Size size = CalculationCellSize.getCellSize(mContext, getItems().size());

        holder.itemView.getLayoutParams().width = size.getWidth();
        holder.itemView.getLayoutParams().height = size.getHeight();
    }

    // STICKY HEADER

//    @Override
//    public long getHeaderId(int position) {
//        return mTableAdapter.getColumnStickyHeaderId(position);
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
//        return mTableAdapter.onCreateColumnStickyViewHolder(parent);
//    }
//
//    @Override
//    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
//        mTableAdapter.onBindColumnStickyViewHolder(holder, position, 0);
//    }
//
//    @Override
//    public void onTranslateHeaderViewHolder(View headerCurrent, View headerNew) {
//
//    }
}