package com.evrencoskun.tableview.helpers;

import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class CalculationCellSize {

    /**
     *
     * @param context
     * @param size - number of elements in a row (RecyclerView.Adapter getItems().size())
     * @return class Size with width and height for Cell and Column header.
     */
    public static Size getCellSize(Context context, int size){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        int width = display.getWidth();
        Log.d("Display", "HolderLayoutParams " + width);
        int height = display.getWidth()/5;
            switch (size){
                case -1:
                case 0:
                case 1:
                case 2:
                    width = (width - (width / 5)) / 2;
                    break;
                case 3:
                    width = (width - (width / 5)) / 3;
                    break;
                default:
                    width = (width - (width / 5)) / 4;
                    break;
            }
            return new Size(width, height);
    }

    /**
     *
     * @param context
     * @return class Size with width and height for Row header and corner.
     */
    public static Size getRowHeaderSize(Context context){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        int width;
        int height = width = display.getWidth()/5;

        return new Size(width, height);
    }
}